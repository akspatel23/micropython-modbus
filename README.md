# micropython-modbus

A MicroPython port of [`modbus-tk`](https://github.com/ljean/modbus-tk). Provides a master-side implementation of the Modbus RTU (serial) protocol. 

Currently, the Modbus TCP and Modbus RTU slave functionality has not been ported - `micropython-modbus` can only act as a Modbus master, using a MicroPython `machine.UART` instance as the core peripheral. 


## Installation

Installing is easy - just copy the `modbus` folder to your MicroPython board. It can live either in the root, or in a `/lib` folder.

The files add up to several kilobytes, so it is more efficient to "freeze" the library into a MicroPython binary image - just place the folder inside the `ports/<board>/modules` folder when building MicroPython from source, then flash to the board as usual.


## Usage

### Configuring the serial peripheral

`micropython-modbus` operates using a pre-configured MicroPython `machine.UART` instance. The module must be configured with the desired baud rate, bit setting, timeout, and parity, for example:

```python
uart = machine.UART(2, 9600, bits=8, parity=None, stop=1, timeout=1000, timeout_char=50)
master = modbus_rtu.RtuMaster(uart)
```

### Performing Modbus commands

Usage is the same as for `modbus-tk`, meaning that aside from initialisation any Python script written for `modbus-tk` should work with `micropython-modbus`. 

For example, to perform a "read input registers" operation from device address `1` and register `0x00` for a length of two words: 

```python
word_tuple = master.execute(1, modbus.defines.READ_INPUT_REGISTERS, 0x00, 2)
```

### Example project

There is currently one example provided, see [`examples/master_main.py`](./examples/master_main.py). This has been tested with a Nucleo STM32F767 board connected through an SP485E RS-485 driver to an Eastron SDM630MCT power meter. 

The SP485E expects CTS to be used, so this has been attached to a GPIO pin and driven using the provided callback (it should also work with the proper hardware pin if configured correctly inside the `machine.UART` instance). The example reads from the meter using the "read input registers" Modbus method, and extracts the Phase 1 voltage and Phase 1 current. 


## License

micropython-modbus: Implementation of Modbus protocol for MicroPython

Based on "Modbus TestKit": https://github.com/ljean/modbus-tk

Copyright (C) 2009, Luc Jean - luc.jean@gmail.com

Copyright (C) 2009, Apidev - http://www.apidev.fr

Copyright (C) 2018, Extel Technologies - https://gitlab.com/extel-open-source

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.

See [LICENSE](./LICENSE) for the full text.